# DD-Songs-Downloader

A downloader for downloading songs from DD-Songs
  
  
# Installation
1. **Download Executable from this project**
[dd-downloader-win.exe](https://gitlab.com/dd.web.songs/dd-songs-downloader/-/blob/main/dd-downloader-win.exe), or
[dd-downloader-macos](https://gitlab.com/dd.web.songs/dd-songs-downloader/-/blob/main/dd-downloader-macos)

2. **Download FFmpeg**
https://github.com/BtbN/FFmpeg-Builds/releases
  
# Usage:
For using the downloader, it is required to have two parameters:
1. playlist key from DD-Songs
2. folder path to FFmpeg you have downloaded from [FFmpeg](https://github.com/BtbN/FFmpeg-Builds/releases)

After unzipping the file from FFmpeg, you should be able to find three executable files
- ffmpeg.exe
- ffplay.exe
- ffprobe.exe  

`path-to-ffmpeg` is the path to these three files.  
`playlist-key` is the key from DD-Songs. For example:  
https://dd-songs.com/custom/playlist-key?s=1  
https://dd-songs.com/custom/8HKaXuALCqfy8knvOmNoZsVuxaCcJqFY9jTg86QwgAs?s=1  
this key is **8HKaXuALCqfy8knvOmNoZsVuxaCcJqFY9jTg86QwgAs**
  
  
**NodeJS**  
`$ node index.js playlist-key path-to-ffmpeg`  
  
Example:  
https://dd-songs.com/custom/8HKaXuALCqfy8knvOmNoZsVuxaCcJqFY9jTg86QwgAs?s=1  
`$ node index.js 8HKaXuALCqfy8knvOmNoZsVuxaCcJqFY9jTg86QwgAs C:/ffmpeg-N-101953-g4e64c8fa29-win64-gpl/bin`  
  
**Executable**  
`$ dd-downloader-win.exe playlist-key path-to-ffmpeg`  
  
Example:  
https://dd-songs.com/custom/8HKaXuALCqfy8knvOmNoZsVuxaCcJqFY9jTg86QwgAs?s=1  
`$ dd-downloader-win.exe 8HKaXuALCqfy8knvOmNoZsVuxaCcJqFY9jTg86QwgAs C:/ffmpeg-N-101953-g4e64c8fa29-win64-gpl/bin`  
