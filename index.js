const fs = require('fs');
const ffmpeg = require('fluent-ffmpeg');
const ytdl = require('ytdl-core');
const fetch = require('node-fetch');

const ytPrefix = "https://www.youtube.com/watch?v=";

const convert = (input, output) => {
    return new Promise((resolve, reject) => {
        try {
            console.log(`Converting source ${input} to ${output}`);
            ffmpeg(input)
                .outputOptions(['-write_xing 0'])
                .output(output)
                .on('end', function () {
                    console.log(`Source conversion done - ${input} to ${output}`)
                    resolve();
                }).on('error', function (err) {
                    console.log('error: ', err);
                    reject(err)
                }).run();
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });
}

const clip = (input, output, start, end) => {
    const tempTitle = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5) + ".mp3";
    return new Promise((resolve, reject) => {
        try {
            console.log(`Clipping from ${input} to ${output} (temp filename: ${tempTitle})`);
            let f = ffmpeg(input)
                .setStartTime(start)
                .outputOptions(['-write_xing 0'])
                .output(tempTitle)
                .on('end', function (err) {
                    if (!err) { console.log(`Clipping Done from ${input} to ${tempTitle}`); }
                    let filename = output.replace(/[\\\/:\*\?"<>\|]/g, "").trim();
                    fs.rename('./' + tempTitle, './' + filename, (err) => { if (err != null) console.log(err) });
                    console.log(`Rename from ${tempTitle} to ${output}`);
                    resolve();
                })
                .on('error', function (err) {
                    console.log('error: ', err)
                    reject(err)
                });
            if (end > 0) {
                f.setDuration(end - start);
            }
            f.run();
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });

}

const goClip = (song) => {
    clip(song.source_id + ".mp3", song.title + ".mp3", song.start, song.end);
}

const download = (song) => {
    console.log(`Current song ${song.title} from ${song.source_id}`);
    const begin = song.start * 1000;
    return new Promise((resolve, reject) => {
        try {
            if (!fs.existsSync('./' + song.source_id + ".mp4")) {
                let info;
                if (song.end === 0) {
                    info = ytdl(ytPrefix + song.source_id, { begin: begin, quality: 'highestaudio' });
                } else {
                    info = ytdl(ytPrefix + song.source_id, { quality: 'highestaudio' });
                }
                let stream = info.pipe(fs.createWriteStream(song.source_id + ".mp4"));
                console.log(`Downloading source - ${song.source_id}.mp4`);
                stream.on('finish', () => {
                    convert(song.source_id + ".mp4", song.source_id + ".mp3").then(() => {
                        goClip(song);
                        resolve();
                    });
                });
            } else if (!fs.existsSync('./' + song.source_id + ".mp3")) {
                convert(song.source_id + ".mp4", song.source_id + ".mp3").then(() => {
                    goClip(song);
                    resolve();
                });
            } else {
                goClip(song);
                resolve();
            }
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });
};

const main = async (key, ffmpegPath = undefined) => {
    const ddSongsHost = "https://dd-songs.com";

    if (key === undefined || key.length === undefined || key.length <= 0) {
        console.log("Invalid params: missing playlist hash key.");
        return;
    }

    if (ffmpegPath !== undefined) {
        ffmpeg.setFfmpegPath(`${ffmpegPath}/ffmpeg.exe`);
        ffmpeg.setFfprobePath(`${ffmpegPath}/ffprobe.exe`);
    }

    await fetch(ddSongsHost + "/api/getSongs/" + key).then(result => result.json().then((async (info) => {
        const playlist = info.songs;
        for (let index = 0; index < playlist.length; index++) {
            const song = playlist[index];
            await download(song);
        }
    })));
}

main(process.argv[2], process.argv[3]);